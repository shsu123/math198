\documentclass[11pt]{article}
\usepackage{fullpage}
\usepackage[left=1in,top=1in,right=1in,bottom=1in,headheight=3ex,headsep=3ex]{geometry}
\usepackage{graphicx}
\usepackage{float}

\newcommand{\blankline}{\quad\pagebreak[2]}

\newcommand{\parr}{\par \vspace{2mm}}

\setlength{\parindent}{0pt}

\title{Math 198 Decal: Algebra in Dimensions Two}
\author{Dennis Chen\footnote{dennisfchen@berkeley.edu}\enspace and Samuel Hsu\footnote{samuelhsu@berkeley.edu}}
\date{Spring 2020}

\usepackage[sc]{mathpazo}
\linespread{1.05} 
\usepackage[T1]{fontenc}
\usepackage[mmddyyyy]{datetime}
\usepackage{advdate}
\newdateformat{syldate}{\twodigit{\THEMONTH}/\twodigit{\THEDAY}}
\newsavebox{\MONDAY}\savebox{\MONDAY}{Mon}
\newcommand{\week}[1]{%
%  \cleardate{mydate}% Clear date
% \newdate{mydate}{\the\day}{\the\month}{\the\year}% Store date
  \paragraph*{\kern-2ex\quad #1, \syldate{\today} - \AdvanceDate[4]\syldate{\today}:}% Set heading  \quad #1
%  \setbox1=\hbox{\shortdayofweekname{\getdateday{mydate}}{\getdatemonth{mydate}}{\getdateyear{mydate}}}%
  \ifdim\wd1=\wd\MONDAY
    \AdvanceDate[7]
  \else
    \AdvanceDate[7]
  \fi%
}
\usepackage{setspace}
\usepackage{multicol}
%\usepackage{indentfirst}
\usepackage{fancyhdr,lastpage}
\usepackage{url}
\pagestyle{fancy}
\usepackage{hyperref}
\usepackage{lastpage}
\usepackage{amsmath}
\usepackage{layout}
\usepackage{comment}

\lhead{Math 198 Decal}
\chead{Algebra in Dimensions Two}

\rhead{Spring 2020}

\lfoot{}
\cfoot{\small \thepage/\pageref*{LastPage}}
\rfoot{}

\usepackage{array, xcolor}
\usepackage{color,hyperref}
%\definecolor{clemsonorange}{HTML}{EA6A20}
\hypersetup{colorlinks,breaklinks,linkcolor=blue,urlcolor=blue,anchorcolor=blue,citecolor=black}

\begin{document}

\maketitle

\tableofcontents

\section{Course Description}

\noindent This may be roughly considered the sequel to ``Conceptual Mathematics" (Spring 2019 Math 98 decal), although if that decal could be considered experimental then this one might be labelled as ambitious. The overarching goal this semester is give an inkling of higher dimensional algebra, two dimensional structures in particular, by introducing higher dimensional structures and their applications. We will spend time talking about applications to geometry, topology, computer science, algebra, physics, and some parts of applied math like the study of diagrams outside of math like Petri nets, control flow diagrams, circuit diagrams, and so on. \parr 

Despite largely stopping short of the “zoo” of structures which live above dimension two, we will heed its other often neglected inhabitants including double categories and proarrow equipments, Yoneda structures, multicategories, and others; there is quite a huge chunk of low dimensional algebra that remains unexplored or at least poorly disseminated. 

\section{Reading}

\begin{itemize}
\item Course notes online at the course webpage:\\ \href{https://www.ocf.berkeley.edu/~samhsu/wiki/shsupublic/show/spring+2020+math+198+decal}{ocf.berkeley.edu/~samhsu/wiki/shsupublic/show/spring+2020+math+198+decal}
\item George Cruttwell and Mike Shulman, A Unified Framework for Generalized Multicategories, 2010, TAC, volume 24. Available at \href{http://www.tac.mta.ca/tac/volumes/24/21/24-21abs.html}{tac.mta.ca/tac/volumes/24/21/24-21abs.html} \emph{(heavily drawn from)}
\item Stephen Lack, A 2-categories Companion, 2010. Available at \href{https://arxiv.org/abs/math/0702535}{arxiv.org/abs/math/0702535} \emph{(often drawn from)}
\item Ivan Di Liberti and Fosco Loregian, Unicity of Formal Category Theory, 2019. Available at \href{https://arxiv.org/abs/1901.01594}{arxiv.org/abs/1901.01594} \emph{(somewhat drawn from)}
\end{itemize}

Large portions of this decal will follow these sources (not in full however). Other references that might be helpful will be given as references in the notes.

\section{Prerequisites/Corequisites}

Familiarity with chapter one through five of Mac Lane's \emph{Categories for the Working Mathematician} or equivalent is advised. Although we will spend some time reviewing enriched categories, notions of algebraic theories, and profunctors, it might be helpful to have seen these notions before. 


\section{Course Structure}

\subsection{Class Structure}

This class will be taught similar to a discussion section. Each week we will meet once for an one and a half hours to talk about readings. \parr


\subsection{Grading Policy}

\begin{itemize}
\item 100\% homework (don't worry, they will be short; graded primarily on completion)
\end{itemize}

The class is graded on a pass/no pass basis. Students should get at least 70\% to pass the course. \parr

\subsection{Accomodations and Emergencies}

If you need an accomodation, please let us know as soon as possible.  \parr 

In case something important or urgent comes up, please contact us as soon as possible and we will try to work something out. 

\section{Schedule and Readings}

The schedule is tentative and subject to change. Since our class time is limited, please at least skim the readings before meeting. \parr

The last three weeks are set aside either for review, as overflow, or for special topics which will depend on how people are feeling. One suggestion up for consideration is a preview of Riehl and Verity's formal categorical framework in what they call $\infty$-cosmoi. Another could be enriched 2-category theory or additional topics from one of our sources e.g. nerves of 2 and double categories. 

\SetDate[20/01/2020]

\week{Week 01} Review of nonsense
\begin{itemize}
\item Enriched categories, functors, natural transformations
\item Weighted limits
\item Operads
\item Lawvere theories
\item PROPS
\item Monads  
\item Monoidal categories
\item Applications of the above items  
\end{itemize}

\week{Week 02} Introduction of some important players
\begin{itemize} 
\item 2-categories
\item Double categories
\item Multicategories
\item Polycategories
  \item Examples 
\end{itemize}

\week{Week 03} Basic theory of 2-categories (how it differs from 1-category theory)
\begin{itemize}
\item Adjunctions in two dimensions
\item Limits in two dimensions
\item Examples: Grothendieck construction, modules over monads, homotopy quotients, etc.
\item Coherence in category theory
\item Applications  
\end{itemize}

\week{Week 04} Some algebra
\begin{itemize}
\item 2-monads
\item Operads in $\underline{\underline{\mathrm{Cat}}}$
\item Algebras in 2-categories
\item 2-Lawvere theories  
\item Examples: categories with various kinds of stuff/structures/property: filtered colimits, topoi, products, monoidal product, etc.
\item Codescent objects
\item Applications  
\end{itemize}

\week{Week 05} Additional players, starting to move towards representability
\begin{itemize}
\item Double multicategories 
\item 2-multicategories 
\item Generalized multicategories: the basic idea, internal to any double category of $T$-spans with $T$ a monad
  \item Applications: Gray tensor product, some basic categories of cobordisms, structures for internalization, graded exponential comonad, context free grammars
\end{itemize}

\week{Week 06} Abstracting out properties of $\underline{\underline{\mathrm{Cat}}}$
\begin{itemize}  
\item Proarrow equipment (important)
\item Yoneda structures
\item (The basic idea of) Yosegi boxes
\item Examples  
\end{itemize}

\week{Week 07} Formal notions of some important concepts
\begin{itemize}
\item Gabriel Ulmer duality
\item Acessibility and presentability  
\item Isbell duality
\item Examples and discussion   
\end{itemize}

\week{Week 08} 
\begin{itemize}
\item Further examples of generalized multicategories  
\item Monads on virtual double categories
\item Opcartesian cells, composites, and units  
\end{itemize}

\week{Week 09} \emph{\textbf{Spring Break}}

\week{Week 10} 
\begin{itemize}
\item 2-categories of $T$-monoids
\item Normalization
\item (Aside) A case study of Cheng's comparison between \textbf{BD}, \textbf{HMP}, and \textbf{L}'s versions of opetopic category.
\end{itemize}

\week{Week 11} 
\begin{itemize}
\item Representability (Major goal)
\item Examples and applications
\end{itemize}

\week{Week 12} 
\begin{itemize}
\item 2-topoi and motivations e.g. stack semantics and cohomology
\item Yoneda structures from 2-topoi
\end{itemize}

\week{Week 13} 
\begin{itemize}
  \item Special topics
\end{itemize}

\week{Week 14} 
\begin{itemize}
  \item Special topics
\end{itemize}

\week{Week 15}
\begin{itemize}
\item Special topics
\end{itemize}




\end{document}
